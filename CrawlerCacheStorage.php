<?php
/**
 * @file
 */

class CrawlerCacheStorage {
  /**
   * Hashed filename
   * @var string
   */
  public $filename;
  /**
   * Stored current filename path
   * @var string
   */
  public $filenamepath;
  /**
   * @var string
   */
  public $filedata;

  /**
   * Hashed domain directory name
   * @var string
   */
  public $domaindir;
  /**
   * Parsed url array
   * @var array
   */
  public $parseurl = array();

  /**
   * Class initialization usually via URL to sitemap.xml
   * @param $url
   */
  public function __construct($url) {
    if ($url != NULL){
      $this->getURLData($url);
    }
    return $this;
  }

  /**
   * @param $url
   */
  public function getURLData($url) {
    $this->filename = hash('md4', $url);
    $this->parseurl = parse_url($url);
    $this->domaindir = hash('md4', $this->parseurl['host']);
    if (!(file_exists(file_directory_temp() . '/crawler/' . $this->domaindir . '/' . $this->filename))) {
      @mkdir(file_directory_temp() . '/crawler/');
      @mkdir(file_directory_temp() . '/crawler/' . $this->domaindir . '/');
      file_put_contents(file_directory_temp() . '/crawler/' . $this->domaindir . '/' . $this->filename, file_get_contents($url));
    }
    $this->filenamepath = file_directory_temp() . '/crawler/' . $this->domaindir . '/' . $this->filename;
    $this->filedata = file_get_contents($this->filenamepath);
    return $this;
  }
} 