<?php
/**
 * @file
 * fcrawl_data.features.taxonomy.inc
 */

/**
 * Implements hook_taxonomy_default_vocabularies().
 */
function fcrawl_data_taxonomy_default_vocabularies() {
  return array(
    'vocabulary_2' => array(
      'name' => 'купівля',
      'machine_name' => 'vocabulary_2',
      'description' => 'оголошення про купівлю в Калуші і калуському районі',
      'hierarchy' => '2',
      'module' => 'taxonomy',
      'weight' => '-6',
    ),
    'vocabulary_3' => array(
      'name' => 'продаж',
      'machine_name' => 'vocabulary_3',
      'description' => 'оголошення про продаж в Калуші і калуському районі',
      'hierarchy' => '2',
      'module' => 'taxonomy',
      'weight' => '-8',
    ),
    'vocabulary_4' => array(
      'name' => 'послуги',
      'machine_name' => 'vocabulary_4',
      'description' => 'оголошення про послуги в Калуші і калуському районі',
      'hierarchy' => '2',
      'module' => 'taxonomy',
      'weight' => '-7',
    ),
    'vocabulary_5' => array(
      'name' => 'працевлаштування',
      'machine_name' => 'vocabulary_5',
      'description' => 'оголошення про працевлаштування в Калуші і калуському районі',
      'hierarchy' => '1',
      'module' => 'taxonomy',
      'weight' => '-10',
    ),
    'vocabulary_6' => array(
      'name' => 'нерухомість',
      'machine_name' => 'vocabulary_6',
      'description' => 'оголошення про нерухомість',
      'hierarchy' => '2',
      'module' => 'taxonomy',
      'weight' => '-9',
    ),
    'vocabulary_8' => array(
      'name' => 'обміняю-подарую',
      'machine_name' => 'vocabulary_8',
      'description' => 'оголошення про обмін і подарунки в Калуші і калуському районі',
      'hierarchy' => '2',
      'module' => 'taxonomy',
      'weight' => '-5',
    ),
  );
}
