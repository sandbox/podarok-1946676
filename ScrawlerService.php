<?php
/**
 * Created by PhpStorm.
 * User: Котёнок
 * Date: 27.10.13
 * Time: 15:17
 */

class ScrawlerService {
  /**
   * @var
   */

  public $config;

  public function __construct($config) {
    $this->config = $config;
  }

  public function saveConfig($config) {
    $this->config = $config;
    return $this;
  }

  public function getConfig() {
    return $this->config;
  }
} 