<?php
/**
 * @file
 */

interface FeedsConfigurableInterface {
  /**
   * Get configuration of this feed.
   */
  public function getConfig();
}