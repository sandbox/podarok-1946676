<?php
/**
 * @file
 */
class FcrawlParser extends FeedsParser {
  /**
   * Parse content fetched by fetcher.
   *
   * Extending classes must implement this method.
   *
   * @param FeedsSource $source
   *   Source information.
   * @param $fetcher_result
   *   FeedsFetcherResult returned by fetcher.
   */
  public function parse(FeedsSource $source, FeedsFetcherResult $fetcher_result) {
    return new FeedsParserResult(array());
  }
}