<?php
/**
 * @file FcrawlHidra.php
 * Implements \Drupal\fcrawl\FcrawlInterface
 */

include_once('FcrawlInterface.php');


class FcrawlHidra extends FeedsImporter implements FcrawlInterface {

  /**
   * @param $id
   */
  function __construct($id) {
    if ($this->isSiteMapLatest($id) && $this->getAllMappings($id) && $this->getTaxonomies($id)) {
      $this->config['state'] = TRUE;
    }
    else {
      $this->config['state'] = FALSE;
    }
    parent::__construct($id);
    $this->config['name'] = $this->config['name'] . '[fcrawl]';
  }

  public function isSiteMapLatest($id) {
    // @todo get state from fetcher
    // feeds_importer($id)->fetcher->config['state'];
    // if FALSE return FALSE and feeds_importer($id)->fetcher->refetchSitemap
    return TRUE;
  }

  public function getDomains($id) {
    // @todo get state from parser
    // feeds_importer($id)->parser->config['state'];
    // if FALSE return array($domains)
    return TRUE;
  }

  public function getTaxonomies($id) {
    // @todo get state from parser
    //feeds_importer($id)->parser->config['state'];
    // if FALSE return array($taxonomies)
    return TRUE;
  }

  public function getAllMappings($id) {
    // @todo get state from parser and fetcher
    // feeds_importer($id)->parser->mappings;
    // @todo possible this not needed for fetcher
    // feeds_importer($id)->fetcher->mappings;
    // feeds_importer($id)->parser->saveMappings;
    // return FALSE if new mappings
    return TRUE;
  }

  public function configForm(&$form_state) {
    $form = parent::configForm($form_state);

    return $form;
  }

  /**
   * @param array $form
   * @return array
   */
  static function getConfigForm($form, &$form_state, $configurable, $form_method) {
// @todo fix it
    $form = $configurable->$form_method($form_state);
    $form['#configurable'] = $configurable;
    $form['#feeds_form_method'] = $form_method;
    $form['#validate'] = array('feeds_form_validate');
    $form['#submit'] = array('feeds_form_submit');
    $form['submit'] = array(
      '#type' => 'submit',
      '#value' => t('Save'),
      '#weight' => 100,
    );
    $form['fcrawl'] = array(
      '#type' => 'fieldset',
      '#title' => t('FcrawlHidra configuration'),
      '#collapsible' => TRUE,
      '#weight' => -100,
    );
    return $form;
  }

}