<?php
/**
 * @file FcrawlInterface.php
 */

/**
 * Class FcrawlInterface
 * @package Drupal\fcrawl
 */
interface FcrawlInterface {

  /**
   * @param $id
   * @return mixed
   */
  public function isSiteMapLatest($id);

  /**
   * @param $id
   * @return mixed
   */
  public function getDomains($id);

  /**
   * @param $id
   * @return mixed
   */
  public function getTaxonomies($id);

  /**
   * @param $id
   * @return mixed
   */
  public function getAllMappings($id);
}