<?php
/**
 * @file
 */

class FcrawlFetcher extends FeedsFetcher {
  /**
   * @var $raw
   * raw data for fetcher
   */
  var $raw;

  function __construct($id) {
    parent::__construct($id);
  }

  /**
   * Fetch content from a source and return it.
   *
   * Every class that extends FeedsFetcher must implement this method.
   *
   * @param $source
   *   Source value as entered by user through sourceForm().
   *
   * @return
   *   A FeedsFetcherResult object.
   */
  public function fetch(FeedsSource $source) {
    return new FeedsFetcherResult($this->raw);
  }

  /**
   * Callback methods, exposes source form.
   */
  public function sourceForm($source_config) {
    return array();
  }

  /**
   * Return configuration form for this object. The keys of the configuration
   * form must match the keys of the array returned by configDefaults().
   *
   * @return
   *   FormAPI style form definition.
   */
  public function configForm(&$form_state) {
    return array();
  }
}