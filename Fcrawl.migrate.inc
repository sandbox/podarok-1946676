<?php
/**
 * @file
 */

class FcrawlMigration extends XMLMigration {
  public function __construct() {
    parent::__construct(array('group_name' => 'fcrawl'));
    $this->description = t('kalushweb testing migration');
    // @todo fixme
    //$this->dependencies = array('WineRegion', 'WineUser');

    // There isn't a consistent way to automatically identify appropriate "fields"
    // from an XML feed, so we pass an explicit list of source fields
    $fields = array(
      'title' => 'VV title',
      'body' => 'VV body',
      'username' => 'VV username',
      'contact' => 'Contact name',
      'phones' => 'VV phones',
      'www' => 'VV www',
      'email' => 'VV e-mail',
    );

    // The source ID here is the one retrieved from the XML listing file, and
    // used to identify the specific item's file
    $this->map = new MigrateSQLMap($this->machineName,
      array(
        'loc' => array(
          'type' => 'varchar',
          'length' => 255,
          'not null' => TRUE,
        )
      ),
      MigrateDestinationNode::getKeySchema()
    );

    // IMPORTANT: Do not try this at home! We have included importable files
    // with the migrate_example module so it can be very simply installed and
    // run, but you should never include any data you want to keep private
    // (especially user data like email addresses, phone numbers, etc.) in the
    // module directory. Your source data should be outside of the webroot, and
    // should not be anywhere where it may get committed into a revision control
    // system.

    // This can also be an URL instead of a file path.
    $xml_folder = DRUPAL_ROOT . '/' . drupal_get_path('module', 'fcrawl') . '/';
    $list_url = $xml_folder . 'kalush.xml';
    // Each ID retrieved from the list URL will be plugged into :id in the
    // item URL to fetch the specific objects.
    $item_url = new FcrawlMigrateListSitemap($list_url);

    // We use the MigrateSourceList class for any source where we obtain the list
    // of IDs to process separately from the data for each item. The listing
    // and item are represented by separate classes, so for example we could
    // replace the XML listing with a file directory listing, or the XML item
    // with a JSON item.
    $this->source = new MigrateSourceList($item_url, new MigrateItemXML($item_url), $fields);

    $this->destination = new MigrateDestinationNode('vv');

    // TIP: Note that for XML sources, in addition to the source field passed to
    // addFieldMapping (the name under which it will be saved in the data row
    // passed through the migration process) we specify the Xpath used to retrieve
    // the value from the XML.
    $this->addFieldMapping('title', 'loc')->callbacks('kalushGetTitle');
    $this->addFieldMapping('body', 'loc')->callbacks('kalushGetBody');
    //var_dump($this->destination->fields());exit;
    

      //->sourceMigration('WineUser')
      //->defaultValue(1);


//    $this->addUnmigratedDestinations(array('revision_uid', 'created', 'changed',
//      'status', 'promote', 'sticky', 'revision', 'log', 'language', 'tnid',
//      'is_new', 'body:summary', 'body:format', 'body:language',
//      'migrate_example_wine_regions:source_type', 'migrate_example_wine_regions:create_term',
//      'comment'));
//    if (module_exists('path')) {
//      $this->addFieldMapping('path')
//        ->issueGroup(t('DNM'));
//      if (module_exists('pathauto')) {
//        $this->addFieldMapping('pathauto')
//          ->issueGroup(t('DNM'));
//      }
//    }
//    if (module_exists('statistics')) {
//      $this->addUnmigratedDestinations(array('totalcount', 'daycount', 'timestamp'));
//    }
  }
}

/**
 * Class FcrawlMigrateListSitemap
 */
class FcrawlMigrateListSitemap extends MigrateListXML{

  /**
   * Load the XML at the given URL, and return an array of the IDs found within it.
   *
   * @return array
   */
  public function getIdList() {
    migrate_instrument_start("Retrieve $this->listUrl");
    $xml = simplexml_load_file($this->listUrl, "SimpleXMLIterator");
    migrate_instrument_stop("Retrieve $this->listUrl");
    //$xml->children('loc');


    if ($xml !== FALSE) {
      return $xml;
    }
    else {
      Migration::displayMessage(t(
        'Loading of !listUrl failed:',
        array('!listUrl' => $this->listUrl)
      ));
      foreach (libxml_get_errors() as $error) {
        Migration::displayMessage(MigrateItemsXML::parseLibXMLError($error));
      }
      return NULL;
    }
  }

  /**
   * Given an XML object, parse out the IDs for processing and return them as an
   * array. The default implementation assumes the IDs are simply the values of
   * the top-level elements - in most cases, you will need to override this to
   * reflect your particular XML structure.
   *
   * @param SimpleXMLElement $xml
   *
   * @return array
   */
  protected function getIDsFromXML(SimpleXMLElement $xml) {
    $ids = array();
    foreach ($xml->children() as $element) {
      $ids[] = (string)$element;
    }
    return array_unique($ids);
  }
}