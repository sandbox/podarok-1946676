<?php
/**
 * @file fcrawl.admin.inc
 */

/**
 * @param $form
 * @param $form_state
 * @return array
 */
function fcrawl_admin_settings_form($form, &$form_state) {
  $form['fcrawl'] = array(
    '#type' => 'fieldset',
    '#title' => t('Fcrawl configuration'),
  );
  $form['fcrawl']['default_class'] = array(
    '#type' => 'checkbox',
    '#title' => t('FcrawlHidra as default class for feeds_importer_class'),
    '#default_value' => variable_get('feeds_importer_class') == 'FcrawlHidra' ? TRUE : FALSE,
  );
  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save default class for feeds_importer_class'),
  );

  return $form;
}

/**
 * @param $form
 * @param $form_state
 */
function fcrawl_admin_settings_form_submit($form, &$form_state) {
  if ($form_state['values']['default_class']) {
    variable_set('fcrawl_class_backup', variable_get('feeds_importer_class', 'FeedsImporter'));
    variable_set('feeds_importer_class', 'FcrawlHidra');
  }
  if ($form_state['values']['default_class'] == NULL) {
    variable_set('feeds_importer_class', 'FeedsImporter');
  }
}